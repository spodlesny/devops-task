# -*- coding: utf-8 -*-
import json

import jwt
from flask import Flask
import psutil

from metrics import config
from metrics.utils import DataInterfaces

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "Hello world"


@app.route("/hello/<name>")
def say_hello(name):
    return f"Hello <b>{name}</b>"


@app.route("/metrics/cpu")
def metrics_cpu():
    load = psutil.cpu_percent(interval=None, percpu=True)
    data = {"soft_interrupts": getattr(psutil.cpu_stats(), "soft_interrupts"),
            "interrupts": getattr(psutil.cpu_stats(), "interrupts"),
            "ctx_switches": getattr(psutil.cpu_stats(), "ctx_switches"),
            "number_of_cores_with_logical": psutil.cpu_count(logical=True),
            "number_of_cores": psutil.cpu_count(logical=False),
            "load": load, }

    if app.debug:
        return json.dumps(data, indent=4, sort_keys=True)
    else:
        return jwt.encode(data, config.SECRET_KEY, algorithm='HS256')


@app.route("/metrics/ram")
def metrics_ram():
    data = {"virtual_memory":  psutil.virtual_memory()._asdict(),
            "swap_memory": psutil.swap_memory()._asdict(), }

    if app.debug:
        return json.dumps(data, indent=4, sort_keys=True)
    else:
        return jwt.encode(data, config.SECRET_KEY, algorithm='HS256')


@app.route("/metrics/disk")
def metrics_disk():
    """
    Note: UNIX usually reserves 5% of the total disk space for the root user.
    Total and used fields on UNIX refer to the overall total and used space, whereas free represents the space available
    for the user and percent represents the user utilization (see source code).
    That is why percent value may look 5% bigger than what you would expect it to be.
    Also note that both 4 values match “df” cmdline utility.
    """
    interface = DataInterfaces()
    data = {"disk_partitions": interface.disk_partitions_interface(psutil.disk_partitions(all=True)),
            "disk_io_counters": interface.disk_io_counters_interface(psutil.disk_io_counters(perdisk=True, nowrap=True)),
            "disk_usage": [], }

    for partition in data["disk_partitions"]:
        try:
            mountpoint = partition["mountpoint"]
            mountpoint_data = {mountpoint: psutil.disk_usage(mountpoint)._asdict()}
            data["disk_usage"].append(mountpoint_data)
        except KeyError:
            pass

    if app.debug:
        return json.dumps(data, indent=4, sort_keys=True)
    else:
        return jwt.encode(data, config.SECRET_KEY, algorithm='HS256')


@app.route("/metrics/network")
def metrics_network():
    interface = DataInterfaces()
    data = {
        "net_io_counters": interface.net_io_counters_interface(psutil.net_io_counters(pernic=True)),
        "net_connections": interface.net_connections_interface(psutil.net_connections(kind="inet")),
        "net_if_addrs": interface.net_if_addrs_interface(psutil.net_if_addrs()),
        "net_if_stats": interface.net_if_stats_interface(psutil.net_if_stats()),
    }

    if app.debug:
        return json.dumps(data, indent=4, sort_keys=True)
    else:
        return jwt.encode(data, config.SECRET_KEY, algorithm='HS256')


@app.route("/metrics/services")
def metrics_services():
    list_of_services = []
    for process in psutil.process_iter():  # src: https://psutil.readthedocs.io/en/latest/#psutil.process_iter
        try:
            process_info = process.as_dict(attrs=['pid', 'name', 'username'])
            list_of_services.append(process_info)
        except psutil.NoSuchProcess:
            pass

    if app.debug:
        return json.dumps(list_of_services, indent=4, sort_keys=True)
    else:
        return jwt.encode({"processes": list_of_services}, config.SECRET_KEY, algorithm='HS256')


if __name__ == '__main__':
    psutil.cpu_percent(interval=None, percpu=True)
    app.run(debug=False, host='0.0.0.0')
