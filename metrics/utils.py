class DataInterfaces:
    def net_io_counters_interface(self, data):
        output = {}
        for key, value in data.items():
            output[key] = value._asdict()
        return output

    @staticmethod
    def net_connections_interface(data):
        output = []
        for item in data:
            output.append(item._asdict())
        return output

    @staticmethod
    def net_if_addrs_interface(data):
        output = {}
        for key, value in data.items():
            items = []
            for item in value:
                items.append(item._asdict())
            output[key] = items

        return output

    def net_if_stats_interface(self, data):
        return self.net_io_counters_interface(data)

    def disk_partitions_interface(self, data):
        return self.net_connections_interface(data)

    def disk_io_counters_interface(self, data):
        return self.net_io_counters_interface(data)
